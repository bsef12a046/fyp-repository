﻿using System.Web;
using System.Web.Mvc;

namespace Omar_EAD_Term_Project
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}