﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omar_EAD_Term_Project.Models;

namespace Omar_EAD_Term_Project.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        database db = new database();
        public ActionResult index()
        {
            return View();
        }

        public ActionResult Manage_Customer()
        {
           List<User> us = db.Users.ToList();
            return View(us);
        }

    }
}
