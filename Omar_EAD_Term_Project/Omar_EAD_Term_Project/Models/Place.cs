//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omar_EAD_Term_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Place
    {
        public Place()
        {
            this.Hotels = new HashSet<Hotel>();
            this.Picture_Place = new HashSet<Picture_Place>();
            this.Pumps = new HashSet<Pump>();
            this.Questions = new HashSet<Question>();
            this.Resturents = new HashSet<Resturent>();
            this.Sites = new HashSet<Site>();
            this.Specialities = new HashSet<Speciality>();
            this.Tracks = new HashSet<Track>();
            this.Trip_Dest_Combo = new HashSet<Trip_Dest_Combo>();
            this.TripInfoes = new HashSet<TripInfo>();
            this.Video_Place = new HashSet<Video_Place>();
        }
    
        public int Id { get; set; }
        public string name { get; set; }
        public Nullable<int> height { get; set; }
        public Nullable<int> cityId { get; set; }
        public Nullable<int> trips { get; set; }
    
        public virtual ICollection<Hotel> Hotels { get; set; }
        public virtual ICollection<Picture_Place> Picture_Place { get; set; }
        public virtual ICollection<Pump> Pumps { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Resturent> Resturents { get; set; }
        public virtual ICollection<Site> Sites { get; set; }
        public virtual ICollection<Speciality> Specialities { get; set; }
        public virtual ICollection<Track> Tracks { get; set; }
        public virtual ICollection<Trip_Dest_Combo> Trip_Dest_Combo { get; set; }
        public virtual ICollection<TripInfo> TripInfoes { get; set; }
        public virtual ICollection<Video_Place> Video_Place { get; set; }
    }
}
