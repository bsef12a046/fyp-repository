//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omar_EAD_Term_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Admin
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string pwd { get; set; }
        public string phno { get; set; }
        public string address { get; set; }
        public string emailpwd { get; set; }
        public Nullable<int> cityid { get; set; }
    
        public virtual City City { get; set; }
    }
}
