//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omar_EAD_Term_Project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Equipment
    {
        public int Id { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public string color { get; set; }
        public string description { get; set; }
        public string warranty { get; set; }
        public string condition { get; set; }
        public Nullable<double> price { get; set; }
        public string availability { get; set; }
    }
}
